path = Dir.getwd
folder = 'lib'

Dir.foreach("#{path}/#{folder}") do |file|
  next if file == '.' || file == '..'
  filename = File.basename(file,File.extname(file))
  autoload filename.capitalize.to_sym, "#{path}/#{folder}/#{filename}"
end
