require_relative 'config/autoload'

class App
  def self.call
    new.seven
  end

  def seven
    Seven
  end
end

App.call
