# Autoload for Mesa

This is a test for [Mesa](http://mesainc.com.br) and was made with [Ruby 2.3.3](http://www.ruby-lang.org)

## Run

```bash
ruby app.rb
```

## Output

```
Zero
Three
Five
One
Two
Four
Six
Seven
```
